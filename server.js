
//includes express
var express = require('express');
var path = require('path');

var app = express();

//static public resources
// app.use(express.static(path.join(__dirname, 'public')))

app.use('/static', express.static('public'));
app.use('/img', express.static('./public/img'));
app.use('/js', express.static('./public/js'));
app.use('/css', express.static('./public/css'));


var $ = require('jquery');
var jsdom = require('jsdom');

//include jquery
// var jsdom = require('jsdom').jsdom
//   , myWindow = jsdom().createWindow()
//   , $ = require('jQuery')
//   , jq = require('jQuery').create()
//   , jQuery = require('jQuery').create(myWindow)
//   ;
// app.use(jquery());
module.exports = app;



//INDEX | HOME
app.get('/', function (req, res) {
   console.log("Got a GET request for the homepage");
   res.sendFile( __dirname + "/" + "home.html" );
})

//USERS
app.get('/users', function (req, res) {
   res.sendFile( __dirname + "/" + "users.html" );
})

//USERS
app.get('/admin', function (req, res) {
   res.sendFile( __dirname + "/" + "admin.html" );
})
    


var server = app.listen(process.env.PORT || 8000, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("Example app listening at http://%s:%s", host, port)
})